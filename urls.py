from django.contrib import admin
from django.urls import path, include

from levit_report import urls as report_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('reports/', include(report_urls)),
]
