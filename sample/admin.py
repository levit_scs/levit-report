from django.contrib import admin

from .models import Person


@admin.register(Person)
class Personadmin(admin.ModelAdmin):
    pass
