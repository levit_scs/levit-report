from factory import Faker
from factory.django import DjangoModelFactory

from ..models import Person


class PersonFactory(DjangoModelFactory):

    class Meta:
        model = Person

    name = Faker('name')
