from django.contrib.contenttypes.models import ContentType
try:
    # Django 2.x
    from django.urls import reverse_lazy
except ImportError:
    from django.core.urlresolvers import reverse_lazy

from levit_report.models import Document

from django_webtest import WebTest

from ..models import Person
from .factories import PersonFactory


class TestViews(WebTest):

    @classmethod
    def setUpTestData(cls):
        super(TestViews, cls).setUpTestData()

        ct = ContentType.objects.get_for_model(Person)
        cls.document = Document(name='report', slug='report', content_type=ct)
        cls.document.source.name = 'media/reports/test.odt'
        cls.document.save()

        cls.record = PersonFactory()
        cls.record.save()

    def test_get_report(self):
        response = self.app.get(reverse_lazy('print', kwargs={
            'slug': self.document.slug,
            'object_id': self.record.pk
        }))

        self.assertEqual(response.status_code, 200, response.body)

    def test_get_bulk(self):
        other_record = PersonFactory()
        other_record.save()

        response = self.app.get('{}?{}'.format(
            reverse_lazy('print', kwargs={'slug': self.document.slug}),
            '&'.join(['ids[]={}'.format(r.pk) for r in [self.record, other_record]])
        ))

        self.assertEqual(response.status_code, 200, response.body)
